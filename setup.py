#!/usr/bin/env python

from distutils.core import setup

setup(
    name="pomodorox",
    version="0.1.0",
    description="Simply utility to use pomodoro methods in a shell.",
    author="Pesko",
    author_email="vegeta@lasalledutemps.fr",
    url="https://www.lasalledutemps.fr",
    packages=[
        "pomodorox",
    ],
    install_requires=[
        "redis",
    ],
    entry_points={
        "console_scripts": [
            "pomodorox = pomodorox.timer:main",
        ]
    },
)
